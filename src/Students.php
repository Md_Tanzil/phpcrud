<?php
namespace Tanzil;
use PDO;
use PDOException;

class Students
{
    private $conn = '';
    public $name = '';
    public $number = '';

    public function __construct()
    {
        try {
        // do what you want……!
            $this->conn = new PDO
            ("mysql:host=localhost;dbname=students","root",
            "Tanzil1234");
            // set the PDO error mode to exception
            $this->conn->setAttribute(PDO::ATTR_ERRMODE,
            PDO::ERRMODE_EXCEPTION);


        }catch(PDOException $e){
        echo $e->getMessage();
        }
    }

    public function index()
    {
        $query = "SELECT * FROM person";
        $stmt = $this->conn->query($query);
        $data = $stmt->fetchAll();
        return $data;
    }
    public function setData(array $data = [])
    {

        $errors = [];

        if(array_key_exists('name', $data) && !empty($data['name'])){
            $this->name = $data['name'];
        }else{
            $errors[] = 'Name required';
        }

        if(array_key_exists('number', $data) && !empty($data['number'])){
            $this->number = $data['number'];
        }else{
            $errors[] = 'Number required';
        }
         if(count($errors)){
            $_SESSION['errors'] = $errors;
            header('location: create.php');
        }else{
            return $this;
        }

    }
    public function store(){
        $query ="INSERT INTO person(name, number) VALUES(:name , :number)";
        $stmt =$this->conn->prepare($query);
        $stmt->execute(array(
        ':name' => $this->name,
        ':number' => $this->number
        ));
        header('Location:index.php');
    }
    public function show($id)
    {
        $sql = 'SELECT * FROM `person` WHERE id='.$id;
        $stmt = $this->conn->query($sql);
        return $stmt->fetch();
    }

    public function update($id)
    {
        $query ="UPDATE person SET name=:name, number=:number where id = ".$id;

        $stmt = $this->conn->prepare($query);

        $stmt->execute(array(
            ':name' => $this->name,
            ':number' => $this->number
        ));
        header('Location:index.php');
    }
    public function delete($id)
    {
        $query ="delete from person where id=".$id;
        $stmt = $this->conn->query($query);
        $stmt->execute();

        header('Location:index.php');
    }
}
?>