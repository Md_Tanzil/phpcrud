<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Home page</title>
</head>
<body>
	<?php  
	require_once "../vendor/autoload.php";
	use Tanzil\Students;
	$studentObj = new Students;
	$students = $studentObj->index();
	
	?>
	<a href="create.php">Add New</a>

	<table border="1">
		<thead>
			<tr>
				<th>Id</th>
				<th>Name</th>
				<th>Phone Number</th>
				<th>Action</th>
				
			</tr>
		</thead>
		<tbody>
			<?php foreach($students as $student){?>
			<tr>
				<td><?php echo $student['id']?></td>
				<td><?php echo $student['name']?></td>
				<td><?php echo $student['number']?></td>
				<td>
				   <a href="edit.php?id=<?php echo $student['id'] ?>">Edit</a> 
                 | <a onclick="return confirm('Are you sure want to delete?')" href="delete.php?id=<?php echo $student['id'] ?>">Delete</a>
				</td>
			</tr>
			<?php }?>
		</tbody>
	</table>

</body>
</html>