<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Edit Student</title>
</head>
<body>

    <?php
    require_once '../vendor/autoload.php';
    use Tanzil\Students;

    $studentObject = new Students;
    $student = $studentObject->show($_GET['id']);
        // echo '<pre>';
        //  print_r($student);
    ?>

    <form action="update.php" method="POST">
        <input type="hidden" name="id" value="<?php echo $student['id'] ?>">
        <input name="name" value="<?php echo $student['name'] ?>" required placeholder=" Name"><br>
        <input name="number" value="<?php echo $student['number'] ?>" placeholder="number"><br>
        <button type="submit">Update</button>
    </form>
</body>
</html>